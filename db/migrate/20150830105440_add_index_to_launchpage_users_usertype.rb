class AddIndexToLaunchpageUsersUsertype < ActiveRecord::Migration
  def change
    add_index :launchpage_users, :usertype
  end
end
