class AddIndexToLaunchpageUsersEmail < ActiveRecord::Migration
  def change
    add_index :launchpage_users, :email, unique: true
  end
end
