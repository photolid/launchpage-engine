class CreateLaunchpageUsers < ActiveRecord::Migration
  def change
    create_table :launchpage_users do |t|
      t.string :email

      t.timestamps null: false
    end
  end
end
