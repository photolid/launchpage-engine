require 'test_helper'

module Launchpage
  class UsersControllerTest < ActionController::TestCase
    setup do
      @routes = Engine.routes
    end

    test "should get new" do
      get :new
      assert_response :success
    end

    test "should get create" do
      get :create
      assert_response :success
    end

    test "should get index" do
      get :index
      assert_response :success
    end

    test "should get export_csv" do
      get :export_csv
      assert_response :success
    end

  end
end
