$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "launchpage/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "launchpage"
  s.version     = Launchpage::VERSION
  s.authors     = ["mazharoddin"]
  s.email       = ["mazharoddin@gmail.com"]
  s.homepage    = "TODO"
  s.summary     = "TODO: Summary of Launchpage."
  s.description = "TODO: Description of Launchpage."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]

  s.add_dependency "rails"
  s.add_dependency "bootstrap-sass"
  s.add_dependency "activeresource"
  s.add_dependency "sass-rails"
  s.add_dependency "uglifier"
  s.add_dependency "coffee-rails"
  s.add_dependency "jquery-rails"
  s.add_dependency "therubyracer"

  s.add_development_dependency "sqlite3"
  s.add_development_dependency "guard"
  s.add_development_dependency "guard-livereload"
  s.add_development_dependency "guard-rspec"
  s.add_development_dependency "rspec-rails"
  s.add_development_dependency "capybara"
  s.add_development_dependency "database_cleaner"
  s.add_development_dependency "shoulda-matchers"
  s.add_development_dependency "factory_girl_rails"
  s.add_development_dependency "dotenv-rails"

end
