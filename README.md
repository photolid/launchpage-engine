= Launchpage


This engine is clone/rewrite of launchpage-rails(https://github.com/codelitt/launchpage-rails). You can easily plug-in this engine to existing rails app and start collecting user emails without tuching exiting app's code.

This engine also can be used along with new rails app, it allows you to nicely seperate registration module from core app.


###How to Install

1. Clone This repo(git clone git@bitbucket.org:photolid/launchpage-engine.git) to /vendor/engine/ in your rails app. (create engine directory if it doesn't exists.
2. Add launchpage engine to Gemfile and install using bundle install (gem 'launchpage', path: '/vendor/engine/launchpage')
3. Run rake db:migrate in your main app.(launchpage engine's migration files wont be copied to main app, instead they run from engine it self)
4. Edit your rails main app config/routes.rb file to mount launchpage engine.(mount Launchpage::Engine => "/launchpage")
5. Refer launchpage-rails git hub page(https://github.com/codelitt/launchpage-rails) for changing SMTP environment.
6. For Demo or For working example, access http://photolid.com/signup page. You can sign up as beta user :)

###Contributing
1. Fork the repo and clone it.
